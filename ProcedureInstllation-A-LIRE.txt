﻿
Android Studio 3.2 a été utilisé pour le développement. Les versions ultérieures devraient aussi
fonctionner. Il suffit d'ouvrir le projet à sa racine (fichier bibliotap) et de démarrer
l'application (app). Le projet a été développé et testé avec un Huawei P20 (API niveau 28). 
Il est nécessaire d'avoir un téléphone avec un API de niveau 23 minimum.

L'AUTHENTIFICATION NE FONCTIONNERA PAS SANS LES 6 ÉTAPES CI-BAS.
----------------------------------------------------------------------------------------------------
Pour utiliser l'API de Google SignIn, il est nécessaire de s'authentifier par le biais d'une clée.
L'application n'étant pas encore en production (release mode), il est nécéssaire de récupérer la clée
d'identification localement afin de pouvoir s'authentifier à l'API de Google.

Voici les étapes nécessaires à l'obtention d'une clée :

1 - Dans Android Studio, cliquez sur Build ---> Generate Signed APK.
2 - Cliquez sur Choose existing... et sélectionnez la store "debug.keystore" dans le fichier bibliotap/keystores.
3 - Les 2 mots de passes sont "android" et l'alias est androiddebugkey.
4 - Cochez Remember passwords et cliquez sur Next.
5 - Comme Build Type, choisissez debug et cochez V1(Jar Signature).
6 - Cliquez sur Finish et attendez la confirmation que tout s'est bien passé. (un petit pop up en bas en droite)

Vous pouvez maintenant vous connecter à votre compte Google et gérer vos étagères.
----------------------------------------------------------------------------------------------------
