package com.bibliotap.search;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bibliotap.adapters.BooksPreviewAdapter;
import com.bibliotap.assets.Keys;
import com.bibliotap.entities.google_books.GoogleBooksRequestResult;
import com.bibliotap.entities.google_books.GoogleBooksQuery;
import com.bibliotap.entities.google_books.GoogleBooksVolume;
import com.bibliotap.entities.retrofit.GoogleBooksApi;
import com.bibliotap.entities.retrofit.RetrofitGoogleBooks;
import com.bibliotap.main.R;
import com.bibliotap.utils.Utils;

import java.util.List;

public class BooksPreview extends AppCompatActivity {

    private GoogleBooksRequestResult mGoogleBooksRequestResult;

    private List<GoogleBooksVolume> mGoogleBooksList;
    private BooksPreviewAdapter booksPreviewAdapter;
    private String mSearchQuery;
    private boolean mLastBooksFetched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_preview);

        if(getIntent().getParcelableExtra("googleRequestResults") != null && getIntent().getStringExtra("searchQuery") != null){
            mGoogleBooksRequestResult = getIntent().getParcelableExtra("googleRequestResults");
            mGoogleBooksList = mGoogleBooksRequestResult.getVolumes();
            mSearchQuery = getIntent().getStringExtra("searchQuery");
        }

        ImageButton backButton = findViewById(R.id.btnBookPreviewBack);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView booksPreviewQuantity = findViewById(R.id.tvBooksPreviewNavTopTitle);
        booksPreviewQuantity.setText(getString(R.string.books_preview_nav_top_title, mGoogleBooksRequestResult.getTotalItems()));

        RecyclerView recyclerView = findViewById(R.id.rvBooksPreview);
        int numberOfColumns = Utils.calculateNoOfColumns(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        booksPreviewAdapter = new BooksPreviewAdapter(this, mGoogleBooksList);
        recyclerView.setAdapter(booksPreviewAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0) {
                    if(!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN ) && !mLastBooksFetched)
                        fetchMoreBooks();
                }
            }
        });
    }

    private void fetchMoreBooks(){
        GoogleBooksApi googleBooksApi = RetrofitGoogleBooks.getInstance().create(GoogleBooksApi.class);

        Call<GoogleBooksRequestResult> getGoogleVolumes =
                googleBooksApi.getMoreGoogleVolumes(mSearchQuery, GoogleBooksQuery.getFetchedFields(), Keys.GOOGLE_BOOKS_API, 40, booksPreviewAdapter.getGoogleBooksVolumeList().size());

        getGoogleVolumes.enqueue(new Callback<GoogleBooksRequestResult>() {
            @Override
            public void onResponse(Call<GoogleBooksRequestResult> call, Response<GoogleBooksRequestResult> response) {
                List<GoogleBooksVolume> newVolumes = response.body().getVolumes();
                if(newVolumes != null) {
                    booksPreviewAdapter.getGoogleBooksVolumeList().addAll(newVolumes);
                    booksPreviewAdapter.notifyItemRangeInserted(booksPreviewAdapter.getItemCount(), newVolumes.size());
                }
                else
                    mLastBooksFetched = true;
            }

            @Override
            public void onFailure(Call<GoogleBooksRequestResult> call, Throwable t) {
                Toast.makeText(BooksPreview.this, R.string.books_preview_error_fetch_more, Toast.LENGTH_LONG).show();
            }
        });
    }
}
