package com.bibliotap.search;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bibliotap.entities.google_books.GoogleBooksQuery;
import com.bibliotap.main.R;

public class SearchResultsNotFoundFragment extends Fragment {

    private static final String SEARCH_BY = "searchBy";
    private static final String SEARCH_VALUE = "searchValue";

    private String mSearchBy;
    private String mSearchValue;

    public SearchResultsNotFoundFragment() {
        // Required empty public constructor
    }

    public static SearchResultsNotFoundFragment newInstance(String searchBy, String searchValue) {
        SearchResultsNotFoundFragment fragment = new SearchResultsNotFoundFragment();
        Bundle args = new Bundle();
        args.putString(SEARCH_BY, searchBy);
        args.putString(SEARCH_VALUE, searchValue);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            mSearchBy = getArguments().getString(SEARCH_BY);
            mSearchValue = getArguments().getString(SEARCH_VALUE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_results_not_found, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ImageButton btnBackButton = getActivity().findViewById(R.id.btnSearchResultsNotFoundBack);
        btnBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        TextView tvNotFoundMessage = getActivity().findViewById(R.id.tvSearchResultsNotFoundMessage);
        tvNotFoundMessage.setText(buildNotFoundMessage());

        Button btnGoogleSearch = getActivity().findViewById(R.id.btnSearchResultsNotFoundGoogleSearch);
        btnGoogleSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                intent.putExtra(SearchManager.QUERY, mSearchValue);
                startActivity(intent);
            }
        });
    }

    public String buildNotFoundMessage(){

        switch(mSearchBy){
            case GoogleBooksQuery.TITLE:
                return getResources().getString(R.string.search_results_not_found_title) + " " + mSearchValue;
            case GoogleBooksQuery.AUTHOR:
                return getResources().getString(R.string.search_results_not_found_author) + " " + mSearchValue;
            case GoogleBooksQuery.PUBLISHER:
                return getResources().getString(R.string.search_results_not_found_publisher) + " " + mSearchValue;
            case GoogleBooksQuery.ISBN:
                return getResources().getString(R.string.search_results_not_found_isbn) + " " + mSearchValue;
            default:
                return getResources().getString(R.string.search_results_not_found_error) + " " + mSearchValue;
        }
    }
}
