package com.bibliotap.search;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.bibliotap.assets.Keys;
import com.bibliotap.entities.retrofit.GoogleBooksApi;
import com.bibliotap.entities.retrofit.RetrofitGoogleBooks;
import com.bibliotap.entities.google_books.GoogleBooksRequestResult;
import com.bibliotap.entities.google_books.GoogleBooksQuery;
import com.bibliotap.main.R;
/*
    Cette activité sert à effectuer une recherche et rediriger l'utilisateur vers
    l'activité / le fragment correspondant.

    Lors de sa création, elle reçoit un type de recherche (titre, auteur, éditeur ou ISBN)
    et une valeur de recherche. Elle effectue ensuite la recherche et redirige l'utilisateur.

 */

public class SearchResults extends AppCompatActivity {

    private String mSearchBy;
    private String mSearchValue;
    private String mSearchQuery;
    private GoogleBooksRequestResult mGoogleBooksRequestResult;
    private int mSelectedFragmentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        if(savedInstanceState != null){
            displayView(mSelectedFragmentId);
        }
        else {
            if(getIntent().getStringExtra("searchBy") != null && getIntent().getStringExtra("searchValue") != null) {
                mSearchBy = getIntent().getStringExtra("searchBy");
                mSearchValue = getIntent().getStringExtra("searchValue");
                mSearchQuery = GoogleBooksQuery.build(mSearchBy, mSearchValue);
            }

            GoogleBooksApi googleBooksApi = RetrofitGoogleBooks.getInstance().create(GoogleBooksApi.class);

            Call<GoogleBooksRequestResult> getGoogleVolumes =
                    googleBooksApi.getGoogleVolumes(mSearchQuery, GoogleBooksQuery.getFetchedFields(), Keys.GOOGLE_BOOKS_API, 40);

            getGoogleVolumes.enqueue(new Callback<GoogleBooksRequestResult>() {
                @Override
                public void onResponse(Call<GoogleBooksRequestResult> call, Response<GoogleBooksRequestResult> response) {
                    mGoogleBooksRequestResult = response.body();

                    if(mGoogleBooksRequestResult.getTotalItems() == 0){
                        mSelectedFragmentId = R.id.fragment_search_results_not_found;
                        displayView(mSelectedFragmentId);
                    }

                    if(mGoogleBooksRequestResult.getTotalItems() == 1){
                        Intent intent = new Intent(getApplicationContext(), BookDetails.class);
                        intent.putExtra("googleVolume", mGoogleBooksRequestResult.getVolumes().get(0));
                        startActivity(intent);
                        finish();
                    }

                    if(mGoogleBooksRequestResult.getTotalItems() > 1){
                        Intent intent = new Intent(getApplicationContext(), BooksPreview.class);
                        intent.putExtra("googleRequestResults", mGoogleBooksRequestResult);
                        intent.putExtra("searchQuery", mSearchQuery);
                        startActivity(intent);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<GoogleBooksRequestResult> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),
                            R.string.error_google_communication_failed, Toast.LENGTH_LONG)
                            .show();
                }
            });
        }
    }

    private void displayView(int selectedFragmentId) {
        Fragment fragment = null;

        switch (selectedFragmentId) {
            case R.id.fragment_search_results_not_found:
                fragment = SearchResultsNotFoundFragment.newInstance(mSearchBy, mSearchValue);
                break;
        }

        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame_search_results, fragment);
            transaction.commit();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if(mSelectedFragmentId != 0){
            outState.putInt("selectedFragmentId", mSelectedFragmentId);
        }
        if(mSearchBy != null && mSearchQuery != null){
            outState.putString("searchBy", mSearchBy);
            outState.putString("searchValue", mSearchValue);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mSelectedFragmentId = savedInstanceState.getInt("selectedFragmentId");
        mSearchBy = savedInstanceState.getString("searchBy");
        mSearchValue = savedInstanceState.getString("searchValue");
    }
}