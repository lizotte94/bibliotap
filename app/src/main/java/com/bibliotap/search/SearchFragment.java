package com.bibliotap.search;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bibliotap.entities.google_books.GoogleBooksQuery;
import com.bibliotap.main.R;

public class SearchFragment extends Fragment {

    private EditText etSearchBar;
    private Integer mSelectedSearchFieldPosition;
    private String mSelectedSearchField;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        etSearchBar = getActivity().findViewById(R.id.etSearchBar);

        Spinner spSearchBy = getActivity().findViewById(R.id.spSearchFields);
        ArrayAdapter<String> fieldsAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item_search, GoogleBooksQuery.getGoogleSearchFieldsList(getActivity()));
        spSearchBy.setAdapter(fieldsAdapter);
        spSearchBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedSearchFieldPosition = parent.getSelectedItemPosition();
                mSelectedSearchField = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button btnSearch = getActivity().findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SearchResults.class);
                intent.putExtra("searchBy", GoogleBooksQuery.getGoogleSearchFieldsMap(v.getContext()).get(mSelectedSearchField));
                intent.putExtra("searchValue", etSearchBar.getText().toString());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if(etSearchBar != null && !etSearchBar.getText().toString().isEmpty())
            outState.putString("searchBar", etSearchBar.getText().toString());

        if(mSelectedSearchFieldPosition != null){
            outState.putInt("selectedSearchField", mSelectedSearchFieldPosition);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mSelectedSearchFieldPosition = savedInstanceState.getInt("selectedSearchField", 0);
            if (savedInstanceState.getString("searchBar") != null) {
                etSearchBar.setText(savedInstanceState.getString("searchBar"));
            }
        }
    }
}
