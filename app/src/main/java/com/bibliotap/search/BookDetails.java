package com.bibliotap.search;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bibliotap.adapters.AddBookAdapter;
import com.bibliotap.assets.Keys;
import com.bibliotap.bookshelves.GoogleBooksToken;
import com.bibliotap.entities.google_books.GoogleBooksQuery;
import com.bibliotap.entities.google_books.GoogleBooksVolume;
import com.bibliotap.entities.google_books.GoogleBooksVolumeInfo;
import com.bibliotap.entities.google_bookshelves.GoogleBookshelf;
import com.bibliotap.entities.google_bookshelves.GoogleBookshelvesRequestResult;
import com.bibliotap.entities.retrofit.GoogleBooksApi;
import com.bibliotap.entities.retrofit.RetrofitGoogleBooks;
import com.bibliotap.main.MainActivity;
import com.bibliotap.main.R;
import com.bibliotap.utils.BookUtils;
import com.bibliotap.utils.RectangleWithText;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BookDetails extends AppCompatActivity implements GoogleBooksToken.AsyncResponse {

    //region Properties
    private static final String GOOGLE_VOLUME = "googleVolume";

    private GoogleBooksVolume mGoogleBooksVolume;
    private GoogleBooksVolumeInfo mVolumeInfo;

    private ImageButton mBackButton;

    private ImageView mBookCover;
    private RectangleWithText mBookCoverNotFound;
    private TextView mTitleAndSubtitle;
    private TextView mAuthors;
    private TextView mPublisherDatePage;
    private Button mAddBook;
    private Button mSeeOnGoogle;
    private String mLinkToGoogle;
    private TextView mDescription;

    private TextView mTableTitle;
    private TextView mTableSubtitle;
    private LinearLayout mTableRowAuthors;
    private TextView mTablePublisher;
    private TextView mTablePublishedDate;
    private TextView mTablePageCount;
    private TextView mTableISBN10;
    private TextView mTableISBN13;
    private TextView mTableLanguage;
    private TextView mTableDescription;

    private String mToken;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        if(getIntent().getParcelableExtra(GOOGLE_VOLUME) != null){
            mGoogleBooksVolume = getIntent().getParcelableExtra(GOOGLE_VOLUME);
            mVolumeInfo = mGoogleBooksVolume.getVolumeInfo();
        }

        attachViews();
        bindResultWithViews();
        formatEmptyFields();

        mAddBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getToken();
            }
        });

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getToken() {
        GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(this);

        if(googleSignInAccount != null)
            new GoogleBooksToken(this, this, googleSignInAccount.getAccount()).execute();
        else
            redirectToConnectionDialog();
    }

    @Override
    public void getTokenCallback(String token) {
        mToken = token;
        fetchBookshelves();
    }

    public void fetchBookshelves() {
        GoogleBooksApi googleBooksApi = RetrofitGoogleBooks.getInstance().create(GoogleBooksApi.class);

        Call<GoogleBookshelvesRequestResult> getGoogleBookshelves =
                googleBooksApi.getGooglebookshelves("Bearer " + mToken, Keys.GOOGLE_BOOKS_API);

        getGoogleBookshelves.enqueue(new Callback<GoogleBookshelvesRequestResult>() {
            @Override
            public void onResponse(Call<GoogleBookshelvesRequestResult> call, Response<GoogleBookshelvesRequestResult> response) {
                GoogleBookshelvesRequestResult bookshelvesRequestResult = response.body();

                if(bookshelvesRequestResult != null)
                    openAddBookDialog(bookshelvesRequestResult.getBookShelves());
                else
                    Toast.makeText(getApplicationContext(),
                        "Erreur lors de la récupération de vos étagères Google.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GoogleBookshelvesRequestResult> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Erreur lors de la récupération de vos étagères Google.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openAddBookDialog(List<GoogleBookshelf> bookshelves){
        final AlertDialog dialog = new AlertDialog.Builder(this).create();

        LayoutInflater inflater = getLayoutInflater();
        dialog.setView(inflater.inflate(R.layout.dialog_add_book_to_bookshelf, null));

        TextView dialogTitle = new TextView(this);
        dialogTitle.setTypeface(dialogTitle.getTypeface(), Typeface.BOLD);
        dialogTitle.setGravity(Gravity.CENTER_HORIZONTAL);
        dialogTitle.setText(getResources().getString(R.string.book_details_add_book_title));
        dialogTitle.setPadding(20,20,20,20);
        dialogTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
        dialog.setCustomTitle(dialogTitle);
        dialog.show();

        final Spinner spBookshelves = dialog.findViewById(R.id.spBookDetailsBookshelves);
        AddBookAdapter mBookshelfAdapter = new AddBookAdapter(this, R.layout.spinner_item_bookshelf, bookshelves);
        spBookshelves.setAdapter(mBookshelfAdapter);

        dialog.findViewById(R.id.btnBookDetailsAddToBookshelf).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoogleBookshelf selectedBookshelf = (GoogleBookshelf)spBookshelves.getSelectedItem();
                addBookToBookshelf(selectedBookshelf);
                dialog.dismiss();
            }
        });
    }

    public void addBookToBookshelf(GoogleBookshelf selectedBookshelf) {
        GoogleBooksApi googleBooksApi = RetrofitGoogleBooks.getInstance().create(GoogleBooksApi.class);

        Call<Void> addVolumeToBookshelf =
                googleBooksApi.addVolumeToBookshelf(selectedBookshelf.getId(), mGoogleBooksVolume.getId(),
                        "Bearer " + mToken, Keys.GOOGLE_BOOKS_API);

        addVolumeToBookshelf.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(getApplicationContext(),
                        R.string.book_details_add_book_success,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(getApplicationContext(),
                        R.string.book_details_add_book_failed,
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void redirectToConnectionDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).create();

        dialog.setTitle(getResources().getString(R.string.book_details_redirect_to_connection_title));
        dialog.setMessage(getResources().getString(R.string.book_details_redirect_to_connection_message));

        dialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getResources().getString(R.string.book_details_redirect_to_connection_button_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.setButton(AlertDialog.BUTTON_POSITIVE,
                getResources().getString(R.string.book_details_redirect_to_connection_button_positive), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(BookDetails.this, MainActivity.class);
                        intent.putExtra("redirectedToConnect", true);
                        startActivity(intent);
                    }
                });

        dialog.show();

        Button btnPositive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        btnPositive.setTextColor(getResources().getColor(R.color.blueBibliotap, null));
        Button btnNegative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        btnNegative.setTextColor(getResources().getColor(R.color.textColorGray, null));
    }

    private void attachViews() {
        mBackButton = findViewById(R.id.btnBookDetailsBack);
        mBookCover = findViewById(R.id.ivBookCover);
        mTitleAndSubtitle = findViewById(R.id.tvBookDetailsTitleAndSubtitle);
        mAuthors = findViewById(R.id.tvAuthors);
        mPublisherDatePage = findViewById(R.id.tvPublisherDatePage);
        mAddBook = findViewById(R.id.btnAddToList);
        mSeeOnGoogle = findViewById(R.id.btnSeeOnGoogle);
        if (BookUtils.googleLinkExists(mVolumeInfo))
            mSeeOnGoogle.setVisibility(View.VISIBLE);
        mDescription = findViewById(R.id.tvDescription);
        mTableTitle = findViewById(R.id.tvBookDetailsTableTitle);
        mTableSubtitle = findViewById(R.id.tvBookDetailsTableSubtitle);
        mTableRowAuthors = findViewById(R.id.tableRowAuthors);
        mTablePublisher = findViewById(R.id.tvBookDetailsTablePublisher);
        mTablePublishedDate = findViewById(R.id.tvBookDetailsTablePublishedDate);
        mTablePageCount = findViewById(R.id.tvBookDetailsTablePageCount);
        mTableISBN10 = findViewById(R.id.tvBookDetailsTableISBN10);
        mTableISBN13 = findViewById(R.id.tvBookDetailsTableISBN13);
        mTableLanguage = findViewById(R.id.tvBookDetailsTableLanguage);
        mTableDescription = findViewById(R.id.tvBookDetailsTableDescription);
    }

    private void bindResultWithViews() {

        if (BookUtils.bookCoverExists(mVolumeInfo))
            Picasso.get()
                    .load(BookUtils.getBookCover(mVolumeInfo))
                    .into(mBookCover);
        else {
            mBookCoverNotFound = new RectangleWithText(this, getResources().getString(R.string.books_preview_cover_not_found));
            mBookCover.setBackground(mBookCoverNotFound);
        }


        mTitleAndSubtitle.setText(formatTitle(), TextView.BufferType.SPANNABLE);
        mAuthors.setText(formatAuthors());
        mPublisherDatePage.setText(formatPublisherDatePage(), TextView.BufferType.SPANNABLE);
        //mAddBook
        mSeeOnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BookUtils.googleLinkExists(mVolumeInfo)) {
                    mLinkToGoogle = BookUtils.getLinkToGoogle(mVolumeInfo);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mLinkToGoogle));
                    startActivity(browserIntent);
                }
            }
        });
        if (mVolumeInfo.getDescription() != null) {
            mDescription.setText(mVolumeInfo.getDescription());
            mTableDescription.setText(mVolumeInfo.getDescription());
        }

        mTableTitle.setText(mVolumeInfo.getTitle());
        mTableSubtitle.setText(mVolumeInfo.getSubTitle());
        if (mTableRowAuthors.getChildCount() == 0)
            buildAuthorsTextViews();

        mTablePublisher.setText(mVolumeInfo.getPublisher());
        mTablePublishedDate.setText(mVolumeInfo.getPublishedDate());
        mTablePageCount.setText(String.valueOf(mVolumeInfo.getPageCount()));
        if (mVolumeInfo.getGoogleBooksIndustryIdentifiers() != null) {
            mTableISBN10.setText(BookUtils.getISBN10(mVolumeInfo));
            mTableISBN13.setText(BookUtils.getISBN13(mVolumeInfo));
        }
        mTableLanguage.setText(formatLanguage());

    }

    private void formatEmptyFields() {
        SpannableStringBuilder spannable = new SpannableStringBuilder();
        int notFoundColor = getResources().getColor(R.color.bookDetailsEmptyField, null);
        String notFoundText = getResources().getString(R.string.book_details_empty);

        if (mVolumeInfo.getDescription() == null) {
            mDescription.setText(notFoundText);
            mDescription.setTextColor(notFoundColor);
            mTableDescription.setText(notFoundText);
            mTableDescription.setTextColor(notFoundColor);
        }

        if (mVolumeInfo.getAuthors() == null) {
            TextView errorMessage = new TextView(this);
            errorMessage.setText(notFoundText);
            errorMessage.setTextColor(notFoundColor);
            mTableRowAuthors.addView(errorMessage);
        }

        if (mVolumeInfo.getPublisher() == null) {
            mTablePublisher.setText(notFoundText);
            mTablePublisher.setTextColor(notFoundColor);
        }

        if (mVolumeInfo.getPublishedDate() == null) {
            mTablePublishedDate.setText(notFoundText);
            mTablePublishedDate.setTextColor(notFoundColor);
        }

        if (mVolumeInfo.getPageCount() == 0) {
            mTablePageCount.setText(notFoundText);
            mTablePageCount.setTextColor(notFoundColor);
        }

        if (mVolumeInfo.getLanguage() == null) {
            mTableLanguage.setText(notFoundText);
            mTableLanguage.setTextColor(notFoundColor);
        }
    }

    private SpannableString formatTitle() {
        StringBuilder sb = new StringBuilder();
        if (mVolumeInfo.getTitle() != null)
            sb.append(mVolumeInfo.getTitle());

        if (mVolumeInfo.getSubTitle() != null && !mVolumeInfo.getSubTitle().isEmpty())
            sb.append(": " + mVolumeInfo.getSubTitle());

        SpannableString formattedTitle = new SpannableString(sb.toString());
        formattedTitle.setSpan(new StyleSpan(Typeface.BOLD), 0, mVolumeInfo.getTitle().length(), 0);

        return formattedTitle;
    }

    private String formatAuthors() {
        StringBuilder sb = new StringBuilder();

        if (mVolumeInfo.getAuthors() != null) {
            for (int i = 0; i < mVolumeInfo.getAuthors().size(); i++) {
                sb.append(mVolumeInfo.getAuthors().get(i));

                if (i < mVolumeInfo.getAuthors().size() - 1)
                    sb.append(", ");
            }
        }

        return sb.toString();
    }

    private void buildAuthorsTextViews() {

        if (mVolumeInfo.getAuthors() != null) {
            for (int i = 0; i < mVolumeInfo.getAuthors().size(); i++) {
                final TextView author = new TextView(this);

                author.setText(mVolumeInfo.getAuthors().get(i));
                author.setTextColor(getResources().getColor(R.color.blueBibliotap, null));

                if (i < mVolumeInfo.getAuthors().size() - 1) {
                    author.setPadding(0, 0, 0, 40);
                }

                author.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(v.getContext(), SearchResults.class);
                        intent.putExtra("searchBy", GoogleBooksQuery.AUTHOR);
                        intent.putExtra("searchValue", author.getText());
                        startActivity(intent);

                    }
                });
                mTableRowAuthors.addView(author);
            }
        }
    }

    private String formatPublisherDatePage() {
        StringBuilder formattedString = new StringBuilder();

        if (mVolumeInfo.getPublisher() != null)
            formattedString.append(mVolumeInfo.getPublisher() + ", ");

        if (mVolumeInfo.getPublishedDate() != null)
            formattedString.append(mVolumeInfo.getPublishedDate() + ", ");

        if (mVolumeInfo.getPageCount() != 0)
            formattedString.append(mVolumeInfo.getPageCount() + " " + getResources().getString(R.string.book_details_page));

        int endWithComma = formattedString.toString().length() - 2;
        if (formattedString.charAt(endWithComma) == ',')
            formattedString.deleteCharAt(endWithComma);

        return formattedString.toString();
    }

    private String formatLanguage() {

        if (mVolumeInfo.getLanguage() != null) {
            switch (mVolumeInfo.getLanguage()) {
                case "fr":
                    return getResources().getString(R.string.lang_fr);
                case "en":
                    return getResources().getString(R.string.lang_en);
                default:
                    return mVolumeInfo.getLanguage();
            }
        }
        return "";
    }
}
