package com.bibliotap.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;

import com.bibliotap.entities.google_books.GoogleBooksVolumeInfo;
import com.bibliotap.entities.google_books.GoogleBooksIndustryIdentifier;
import com.bibliotap.entities.google_bookshelves.GoogleBookshelf;
import com.bibliotap.main.R;

import java.util.Locale;

/*
    Cette classe utilitaire permet d'encapsuler des opérations et vérifications nécéssaires
    lors de l'affichage de données d'un livre, afin de ne pas lever d'exception (notamment le fameux NPE)
    et d'effectuer du formattage de texte. Ça allège de beaucoup les classes concernées et offre une réutilisabilité.
 */

public class BookUtils {

    public static final int BOOK_COVER_WIDTH = 110;
    public static final int BOOK_COVER_HEIGHT = 160;

    public static String getISBN10(GoogleBooksVolumeInfo volumeInfo) {
        for(GoogleBooksIndustryIdentifier googleBooksIndustryIdentifier : volumeInfo.getGoogleBooksIndustryIdentifiers()){
            if(googleBooksIndustryIdentifier.getType().equals("ISBN_10"))
                return googleBooksIndustryIdentifier.getIdentifier();
        }
        return "";
    }

    public static String getISBN13(GoogleBooksVolumeInfo volumeInfo) {
        for(GoogleBooksIndustryIdentifier googleBooksIndustryIdentifier : volumeInfo.getGoogleBooksIndustryIdentifiers()){
            if(googleBooksIndustryIdentifier.getType().equals("ISBN_13"))
                return googleBooksIndustryIdentifier.getIdentifier();
        }
        return "";
    }

    public static String getBookCover(GoogleBooksVolumeInfo volumeInfo) {
        if(volumeInfo.getImageLinks().getThumbnail() != null)
            return volumeInfo.getImageLinks().getThumbnail();

        return volumeInfo.getImageLinks().getSmallThumbnail();
    }

    public static String getLinkToGoogle(GoogleBooksVolumeInfo volumeInfo){
        if(volumeInfo.getPreviewLink() != null)
            return volumeInfo.getPreviewLink();

        return volumeInfo.getInfoLink();
    }

    public static boolean bookCoverExists(GoogleBooksVolumeInfo volumeInfo) {
        return volumeInfo.getImageLinks() != null;
    }

    public static boolean googleLinkExists(GoogleBooksVolumeInfo volumeInfo) {
        return volumeInfo.getPreviewLink() != null || volumeInfo.getInfoLink() != null;
    }

    public static SpannableString formatTitleAndSubtitle(GoogleBooksVolumeInfo volumeInfo){
        StringBuilder sb = new StringBuilder();

        if(volumeInfo.getTitle() != null)
            sb.append(volumeInfo.getTitle());

        if(volumeInfo.getSubTitle() != null && !volumeInfo.getSubTitle().isEmpty()) {
            sb.append(": ");
            sb.append(volumeInfo.getSubTitle());
        }

        if(volumeInfo.getTitle() != null){
            SpannableString formattedTitle = new SpannableString(sb.toString());
            formattedTitle.setSpan(new StyleSpan(Typeface.BOLD), 0, volumeInfo.getTitle().length(), 0);
            return formattedTitle;
        }

        if(volumeInfo.getSubTitle() != null && !volumeInfo.getSubTitle().isEmpty())
            return new SpannableString(volumeInfo.getSubTitle());

        return new SpannableString("");
    }

    public static String formatAuthors(GoogleBooksVolumeInfo volumeInfo){
        StringBuilder sb = new StringBuilder();

        if (volumeInfo.getAuthors() != null) {
            for(int i = 0; i < volumeInfo.getAuthors().size(); i++){
                sb.append(volumeInfo.getAuthors().get(i));

                if(i < volumeInfo.getAuthors().size() - 1)
                    sb.append(", ");
            }
        }
        return sb.toString();
    }

    public static String getBookshelfTitle(Context context, GoogleBookshelf googleBookshelf) {

        Locale currentLang = context.getResources().getConfiguration().locale;

        if(currentLang.getDisplayLanguage().contains("en")){
            return googleBookshelf.getTitle();
        }
        else {
            switch (googleBookshelf.getId()) {
                case 0:
                    return context.getString(R.string.bookshelf_title_favorites);
                case 1:
                    return context.getString(R.string.bookshelf_title_purchased);
                case 2:
                    return context.getString(R.string.bookshelf_title_to_read);
                case 3:
                    return context.getString(R.string.bookshelf_title_reading_now);
                case 4:
                    return context.getString(R.string.bookshelf_title_have_read);
                case 5:
                    return context.getString(R.string.bookshelf_title_reviewed);
                case 6:
                    return context.getString(R.string.bookshelf_title_recently_viewed);
                case 7:
                    return context.getString(R.string.bookshelf_title_my_ebooks);
                case 8:
                    return context.getString(R.string.bookshelf_title_books_for_you);
                case 9:
                    return context.getString(R.string.bookshelf_title_browsing_history);
                default:
                    return googleBookshelf.getTitle();
            }
        }
    }
}
