package com.bibliotap.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.Rect;

import com.bibliotap.main.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/*
    Cette classe permet de créer des placeholders pour les premières de couvertures.
    Android ne fournit pas la possibilité d'ajouter du texte facilement par dessus des Drawable,
    j'ai donc créer cette classe pour pouvoir afficher un texte du type "Première de couverture non disponible"
    par dessus le rectangle qui fait office de couverture de livre vide.
 */

public class RectangleWithText extends Drawable {

    private Context mContext;
    private Paint mRectPaint;
    private Paint mTextPaint;
    private Rect mRect;
    private float mStrokeWidth;
    private String mText;

    private int mCanvasWidth;
    private int mCanvasHeight;

    public RectangleWithText(Context context, String text){
        mContext = context;
        mRectPaint = new Paint();
        mTextPaint = new Paint();
        mRect = new Rect();
        mText = text;
        mStrokeWidth = 4f;
    }

    @Override
    public void draw(@NonNull Canvas canvas) {

        mCanvasWidth = canvas.getClipBounds().width();
        mCanvasHeight = canvas.getClipBounds().height();

        mRectPaint.setStyle(Paint.Style.STROKE);
        mRectPaint.setStrokeWidth(mStrokeWidth);
        mRectPaint.setColor(mContext.getResources().getColor(R.color.textColorGray, null));

        mRect.set(0, 0, mCanvasWidth - ((int) mStrokeWidth * 2), mCanvasHeight - ((int) mStrokeWidth * 2));
        canvas.drawRect(mRect, mRectPaint);

        mTextPaint.setTextSize(20f);
        mTextPaint.setTypeface(Typeface.create("Roboto",Typeface.BOLD));
        mTextPaint.setColor(mContext.getResources().getColor(R.color.textColorGray, null));
        mTextPaint.setElegantTextHeight(true);

        int lineSpacingY = 25;
        int textPositionY = (mCanvasHeight / 2) - (lineSpacingY * splitLines(mText).length);

        for(String line : mText.split("\n")){
            canvas.drawText(line, (mCanvasWidth - mTextPaint.measureText(line)) / 2, textPositionY + lineSpacingY, mTextPaint);
            lineSpacingY += 25;

        }
    }

    private String[] splitLines(String text) {
        return text.split("\n");
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }
}
