package com.bibliotap.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.widget.Button;
import android.widget.TextView;

import com.bibliotap.main.R;

import androidx.appcompat.app.AlertDialog;

/*
    Classe utilitaire permettant d'encapsuler quelques opérations nécessaires, allégeant les classes qui les utilisent.
    Le nom des méthodes parlent d'eux-mêmes.
 */

public class Utils {

    public static int getActivityWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int getActivityHeight(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    // Utilisé pour le GridLayout.
    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return (int) (dpWidth / BookUtils.BOOK_COVER_WIDTH);
    }

    public static void openDialog(Activity activity, String title, String message, String buttonMessage){
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();

        TextView tvTitle = new TextView(activity);
        tvTitle.setText(title);
        tvTitle.setPadding(40,40,40,40);
        tvTitle.setTextColor(Color.BLACK);
        tvTitle.setTextSize(20);
        tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
        alertDialog.setCustomTitle(tvTitle);

        TextView tvMessage = new TextView(activity);
        tvMessage.setText(message);
        tvMessage.setPadding(40,0,40,40);
        tvMessage.setGravity(Gravity.START);
        tvMessage.setTextColor(Color.BLACK);
        tvMessage.setForegroundGravity(Gravity.START);
        alertDialog.setView(tvMessage);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, buttonMessage, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();

        Button btn = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        btn.setTextColor(activity.getResources().getColor(R.color.blueBibliotap, null));
    }
}
