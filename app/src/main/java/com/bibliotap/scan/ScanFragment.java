package com.bibliotap.scan;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.bibliotap.entities.google_books.GoogleBooksQuery;
import com.bibliotap.main.R;
import com.bibliotap.search.SearchResults;
import com.bibliotap.utils.Utils;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

/*
    Ce fragment s'occupe de gérer la caméra et le détecteur.
    Lorsqu'un livre est scanné, il redirige l'utilisateur vers l'activité SearchResults
    qui s'occupera de rediriger l'utilisateur vers l'activité ou le fragment correspondant, soit :

        - Aucun résultat trouvé --> SearchResultsNotFoundFragment
        - 1 livre trouvé --> BookDetails
        - Plusieurs livres trouvés --> BooksPreview

     Lors de la création de l'activité (onActivityCreated), la SurfaceView est créée,
     elle servira à "contenir" la caméra. On vérifie si l'accès à la caméra est autorisé.
     Si oui, on démarre la caméra lorsque la surface est créée (SurfaceHolder.Callback2).
     Si non, on demande l'autorisation d'accéder à la caméra.

     Ensuite, au démarrage du fragment (onStart), on démarre le détecteur en spécifiant le type
     de code-barre que nous souhaitons capter (ici EAN_13) et passons son
     instance à la caméra (CameraSource) afin que celle-ci envoi des frames (25 fois par seconde
     dans ce cas-ci) au déteteur. Le détecteur recoit ces Frames dans sa méthode "receiveDetections".
     On vérifie donc, 25 fois par seconde, s'il a détecter un code-barre (mDetectedBarcodes.size() > 0).

     Finalement, dès qu'il détecte un code-barre, on release le détecteur (pour éviter que receiveDetections
     ne soit appellée en boucle) et on démarre l'activité SearchResults en passant le ISBN détecté.

 */

public class ScanFragment extends Fragment {

    private static final int CAMERA_PERMISSION = 666;

    private SurfaceView svBarcode;

    private BarcodeDetector mDetector;
    private CameraSource mCameraSource;
    private SparseArray<Barcode> mDetectedBarcodes;

    public ScanFragment() {
        // Required empty public constructor
    }

    public static ScanFragment newInstance() {
        ScanFragment fragment = new ScanFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scan, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        svBarcode = getActivity().findViewById(R.id.svBarcode);
        svBarcode.getHolder().addCallback(new SurfaceHolder.Callback2() {
            @SuppressLint("MissingPermission")
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                holder.setFixedSize(50, 50);
                if(cameraAccessIsGranted()) {
                    try {
                        mCameraSource.start(holder);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    askForCameraAccess();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                mCameraSource.stop();
            }

            @Override
            public void surfaceRedrawNeeded(SurfaceHolder holder) {}
        });
    }


    @Override
    public void onStart() {
        super.onStart();

        mDetector = new BarcodeDetector.Builder(getContext())
                .setBarcodeFormats(Barcode.EAN_13)
                .build();

        mDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {}

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                mDetectedBarcodes = detections.getDetectedItems();
                if(mDetectedBarcodes.size() > 0){
                    mDetector.release();

                    Intent intent = new Intent(getActivity(), SearchResults.class);
                    intent.putExtra("searchBy", GoogleBooksQuery.ISBN);
                    intent.putExtra("searchValue", mDetectedBarcodes.valueAt(0).displayValue);
                    startActivity(intent);
                }
            }
        });

        mCameraSource = new CameraSource.Builder(getContext(), mDetector)
                .setRequestedPreviewSize(Utils.getActivityWidth(getActivity()), Utils.getActivityHeight(getActivity()))
                .setRequestedFps(25f).setAutoFocusEnabled(true).build();
    }

    /*
        On ajoute le SupressLint car à ce point-ci, nous sommes certain d'avoir obtenu l'autorisation
        lorsque mCameraSource.start() est appelé.
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == CAMERA_PERMISSION){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                try {
                    mCameraSource.start(svBarcode.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED){
                    Utils.openDialog(getActivity(),
                            getResources().getString(R.string.func_camera_access_denied_title),
                            getResources().getString(R.string.func_camera_access_denied_message),
                            getResources().getString(R.string.button_i_understood));
            }
        }
    }

    /*
        On s'assure de bien détruire libérer le Detector et la CameraSource lorsque l'activité est détruite.
        Sinon, il risque fortement d'avoir des erreurs lorsque l'activité sera recréée (lors de la rotation
        du téléphone par exemple).
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mDetector != null)
            mDetector.release();

        if(mCameraSource != null){
            mCameraSource.stop();
            mCameraSource.release();
        }
    }

    private boolean cameraAccessIsGranted() {
        return ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void askForCameraAccess() {
        this.requestPermissions(
                new String[]{Manifest.permission.CAMERA},
                CAMERA_PERMISSION);
    }
}
