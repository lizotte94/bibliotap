package com.bibliotap.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bibliotap.assets.Keys;
import com.bibliotap.bookshelves.Bookshelf;
import com.bibliotap.bookshelves.GoogleBooksToken;
import com.bibliotap.entities.google_books.GoogleBooksVolume;
import com.bibliotap.entities.google_books.GoogleBooksVolumeInfo;
import com.bibliotap.entities.google_bookshelves.GoogleBookshelf;
import com.bibliotap.entities.retrofit.GoogleBooksApi;
import com.bibliotap.entities.retrofit.RetrofitGoogleBooks;
import com.bibliotap.main.R;
import com.bibliotap.search.BookDetails;
import com.bibliotap.utils.BookUtils;
import com.bibliotap.utils.RectangleWithText;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
    Le ViewHolder pattern est utilisé afin de diminuer les appels couteux à FindViewById.
    Puisque les Views sont réutilisées, récupérer un BooksPreviewViewHolder existant et
    modifier son contenu est beaucouo plus efficace que de créer des nouvelles View avec
    findViewById.
 */

public class BookshelfAdapter extends Adapter<BookshelfAdapter.BookshelfViewHolder> {

    private GoogleBookshelf mGoogleBookshelf;
    private List<GoogleBooksVolume> mGoogleBooksVolumeList;
    private Context mContext;

    public BookshelfAdapter(Context context, GoogleBookshelf googleBookshelf, List<GoogleBooksVolume> googleBooksVolumeList) {
        mGoogleBookshelf = googleBookshelf;
        mContext = context;

        if(googleBooksVolumeList != null)
        mGoogleBooksVolumeList = googleBooksVolumeList;
        else
         mGoogleBooksVolumeList = new ArrayList<>();

    }

    @NonNull
    @Override
    public BookshelfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.bookshelf_item, parent, false);

        return new BookshelfViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BookshelfViewHolder holder, int position) {
        GoogleBooksVolumeInfo volumeInfo = mGoogleBooksVolumeList.get(position).getVolumeInfo();

        holder.bookCover.setImageDrawable(null);
        if(BookUtils.bookCoverExists(volumeInfo)){
            Picasso.get()
                    .load(BookUtils.getBookCover(volumeInfo))
                    .into(holder.bookCover);

            holder.bookCover.setTag(mGoogleBooksVolumeList.get(position));
        }
        else{
            RectangleWithText bookCoverNotFound = new RectangleWithText(holder.bookCover.getContext(),
                    holder.bookCover.getContext().getResources().getString(R.string.books_preview_cover_not_found));

            holder.bookCover.setImageDrawable(bookCoverNotFound);
        }

        holder.title.setText(BookUtils.formatTitleAndSubtitle(volumeInfo));
        holder.authors.setText(BookUtils.formatAuthors(volumeInfo));

    }

    @Override
    public int getItemCount() {
        return mGoogleBooksVolumeList.size();
    }


    class BookshelfViewHolder extends RecyclerView.ViewHolder implements GoogleBooksToken.AsyncResponse {

        private ImageView bookCover;
        private TextView title;
        private TextView authors;
        private ImageButton btnDelete;
        private int position;

        BookshelfViewHolder(@NonNull View itemView) {
            super(itemView);

            bookCover = itemView.findViewById(R.id.ivBookshelfBookCover);
            title = itemView.findViewById(R.id.tvBookshelfTitle);
            authors = itemView.findViewById(R.id.tvBookshelfAuthors);
            btnDelete = itemView.findViewById(R.id.btnBookshelfDelete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    position = getAdapterPosition();
                    Intent intent = new Intent(v.getContext(), BookDetails.class);
                    intent.putExtra("googleVolume", mGoogleBooksVolumeList.get(position));
                    v.getContext().startActivity(intent);
                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    position = getAdapterPosition();
                    openDeleteConfirmationDialog(v);
                }
            });
        }

        private void openDeleteConfirmationDialog(View v){
            AlertDialog.Builder dialog = new AlertDialog.Builder(v.getContext());

            dialog.setTitle(R.string.bookshelf_delete_dialog_title);
            dialog.setMessage(v.getContext().getResources()
                    .getString(R.string.bookshelf_delete_dialog_message, mGoogleBooksVolumeList.get(position).getVolumeInfo().getTitle()));

            dialog.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            dialog.setPositiveButton(R.string.button_confirm, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getToken();
                }
            });

            dialog.create();
            dialog.show();
        }

        private void getToken(){
            new GoogleBooksToken(this, (Activity)itemView.getContext(),
                    GoogleSignIn.getLastSignedInAccount(itemView.getContext()).getAccount()).execute();
        }

        @Override
        public void getTokenCallback(String token) {
            deleteBookFromBookshelf(token);
        }

        private void deleteBookFromBookshelf(String token) {

            GoogleBooksApi googleBooksApi = RetrofitGoogleBooks.getInstance().create(GoogleBooksApi.class);

            Call<Void> removeVolumeFromBookshelf =
                    googleBooksApi.removeVolumeFromBookshelf(mGoogleBookshelf.getId(),
                            mGoogleBooksVolumeList.get(position).getId(), "Bearer " + token, Keys.GOOGLE_BOOKS_API);

            removeVolumeFromBookshelf.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    mGoogleBooksVolumeList.remove(mGoogleBooksVolumeList.get(position));
                    ((Bookshelf)mContext).setNavTopTitle();
                    notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Toast.makeText(itemView.getContext(), R.string.bookshelf_delete_error, Toast.LENGTH_SHORT).show();
                }
            });


        }


    }
}
