package com.bibliotap.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bibliotap.entities.google_books.GoogleBooksVolume;
import com.bibliotap.entities.google_books.GoogleBooksVolumeInfo;
import com.bibliotap.main.R;
import com.bibliotap.search.BookDetails;
import com.bibliotap.utils.BookUtils;
import com.bibliotap.utils.RectangleWithText;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
/*
    Le ViewHolder pattern est utilisé afin de diminuer les appels couteux à FindViewById.
    Puisque les Views sont réutilisées, récupérer un BooksPreviewViewHolder existant et
    modifier son contenu est beaucouo plus efficace que de créer des nouvelles View avec
    findViewById.
 */

public class BooksPreviewAdapter extends RecyclerView.Adapter<BooksPreviewAdapter.BooksPreviewViewHolder>{

    private List<GoogleBooksVolume> mGoogleBooksVolumeList;
    private LayoutInflater mInflater;

    public List<GoogleBooksVolume> getGoogleBooksVolumeList() {
        return mGoogleBooksVolumeList;
    }

    public BooksPreviewAdapter(Context context, List<GoogleBooksVolume> googleBooksVolumes) {
        mInflater = LayoutInflater.from(context);
        mGoogleBooksVolumeList = googleBooksVolumes;
    }

    @NonNull
    @Override
    public BooksPreviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.books_preview_item, parent, false);
        return new BooksPreviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BooksPreviewViewHolder holder, int position) {
        GoogleBooksVolumeInfo volumeInfo = mGoogleBooksVolumeList.get(position).getVolumeInfo();

        holder.bookCover.setImageDrawable(null);
        if(BookUtils.bookCoverExists(volumeInfo)){
            Picasso.get()
                    .load(BookUtils.getBookCover(volumeInfo))
                    .into(holder.bookCover);

            holder.bookCover.setTag(mGoogleBooksVolumeList.get(position));
        }
        else{
            RectangleWithText bookCoverNotFound = new RectangleWithText(holder.bookCover.getContext(), holder.bookCover.getContext().getResources().getString(R.string.books_preview_cover_not_found));

            holder.bookCover.setImageDrawable(bookCoverNotFound);
        }

        holder.titleAndSubtitle.setText(BookUtils.formatTitleAndSubtitle(volumeInfo));
        holder.authors.setText(BookUtils.formatAuthors(volumeInfo));
    }

    @Override
    public int getItemCount() {
        return mGoogleBooksVolumeList.size();
    }

    class BooksPreviewViewHolder extends RecyclerView.ViewHolder {

        ImageView bookCover;
        TextView titleAndSubtitle;
        TextView authors;

        BooksPreviewViewHolder(@NonNull View itemView) {
            super(itemView);
            bookCover = itemView.findViewById(R.id.ivBooksPreviewBookCover);
            titleAndSubtitle = itemView.findViewById(R.id.tvBooksPreviewBookTitleAndSubtitle);
            authors = itemView.findViewById(R.id.tvBooksPreviewBookAuthors);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(v.getContext(), BookDetails.class);
                    intent.putExtra("googleVolume", mGoogleBooksVolumeList.get(position));
                    v.getContext().startActivity(intent);
                }
            });
        }
    }
}


