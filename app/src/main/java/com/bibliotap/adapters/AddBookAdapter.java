package com.bibliotap.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bibliotap.entities.google_bookshelves.GoogleBookshelf;
import com.bibliotap.main.R;
import com.bibliotap.utils.BookUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AddBookAdapter extends ArrayAdapter<GoogleBookshelf> {

    private List<GoogleBookshelf> mGoogleBookshelves;

    private LayoutInflater mInflater;
    private Context mContext;

    private ImageView mBookshelfAccess;
    private TextView mBookshelfTitle;

    public AddBookAdapter(@NonNull Context context, int resource, @NonNull List<GoogleBookshelf> googleBookshelves) {
        super(context, resource, googleBookshelves);

        mContext = context;
        mGoogleBookshelves = googleBookshelves;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null)
            convertView = mInflater.inflate(R.layout.spinner_item_bookshelf, parent, false);

        GoogleBookshelf bookshelf = mGoogleBookshelves.get(position);

        mBookshelfAccess = convertView.findViewById(R.id.ivBookDetailsSpinnerBookshelfAccess);
        mBookshelfAccess.setImageDrawable(formatAccess(bookshelf));

        mBookshelfTitle = convertView.findViewById(R.id.tvBookDetailsSpinnerBookshelfTitle);
        mBookshelfTitle.setText(mContext.getString(
                R.string.book_details_add_book_spinner_item, BookUtils.getBookshelfTitle(mContext, bookshelf), bookshelf.getVolumeCount()));

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null)
            convertView = mInflater.inflate(R.layout.spinner_item_bookshelf, parent, false);

        GoogleBookshelf bookshelf = mGoogleBookshelves.get(position);

        mBookshelfAccess = convertView.findViewById(R.id.ivBookDetailsSpinnerBookshelfAccess);
        mBookshelfAccess.setImageDrawable(formatAccess(bookshelf));

        mBookshelfTitle = convertView.findViewById(R.id.tvBookDetailsSpinnerBookshelfTitle);
        mBookshelfTitle.setText(mContext.getString(
                R.string.book_details_add_book_spinner_item, BookUtils.getBookshelfTitle(mContext, bookshelf), bookshelf.getVolumeCount()));

        return convertView;
    }

    @Override
    public int getCount() {
        return mGoogleBookshelves.size();
    }

    @Nullable
    @Override
    public GoogleBookshelf getItem(int position) {
        return mGoogleBookshelves.get(position);
    }

    private Drawable formatAccess(GoogleBookshelf googleBookshelf) {
        switch (googleBookshelf.getAccess()){
            case "PUBLIC":
                return mContext.getResources().getDrawable(R.drawable.ic_public, null);
            case "PRIVATE":
                return mContext.getResources().getDrawable(R.drawable.ic_private, null);
            default:
                return new ColorDrawable(Color.TRANSPARENT);
        }

    }
}
