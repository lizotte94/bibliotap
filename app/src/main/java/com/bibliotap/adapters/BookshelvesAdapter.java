package com.bibliotap.adapters;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bibliotap.bookshelves.Bookshelf;
import com.bibliotap.entities.google_bookshelves.GoogleBookshelf;
import com.bibliotap.main.R;
import com.bibliotap.utils.BookUtils;

import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/*
    Le ViewHolder pattern est utilisé afin de diminuer les appels couteux à FindViewById.
    Puisque les Views sont réutilisées, récupérer un BooksPreviewViewHolder existant et
    modifier son contenu est beaucouo plus efficace que de créer des nouvelles View avec
    findViewById.
 */

public class BookshelvesAdapter extends ArrayAdapter<GoogleBookshelf> {

    private Context mContext;
    private List<GoogleBookshelf> mGoogleBookshelves;

    public BookshelvesAdapter(@NonNull Context context, int resource, @NonNull List<GoogleBookshelf> googleBookshelves) {
        super(context, resource, googleBookshelves);
        mContext = context;
        mGoogleBookshelves = googleBookshelves;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        BookshelvesViewHolder viewHolder;

        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.bookshelves_item, parent, false);
            viewHolder = new BookshelvesViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (BookshelvesViewHolder) convertView.getTag();
        }

        final GoogleBookshelf googleBookshelf = getItem(position);

        if(googleBookshelf != null){
            viewHolder.bookshelfHeader.setText(formatHeader(googleBookshelf));
            viewHolder.bookshelfAccess.setImageDrawable(formatAccess(googleBookshelf));
            viewHolder.bookshelfDescription.setText(googleBookshelf.getDescription());
            viewHolder.llBookshelfDescription.setVisibility(View.VISIBLE);

            if(googleBookshelf.getDescription() == null || googleBookshelf.getDescription().isEmpty())
                viewHolder.llBookshelfDescription.setVisibility(View.GONE);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Bookshelf.class);
                intent.putExtra("googleBookshelf", googleBookshelf);
                v.getContext().startActivity(intent);
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return mGoogleBookshelves.size();
    }

    @Nullable
    @Override
    public GoogleBookshelf getItem(int position) {
        return mGoogleBookshelves.get(position);
    }

    private Drawable formatAccess(GoogleBookshelf googleBookshelf) {
        switch (googleBookshelf.getAccess()){
            case "PUBLIC":
                return mContext.getResources().getDrawable(R.drawable.ic_public, null);
            case "PRIVATE":
                return mContext.getResources().getDrawable(R.drawable.ic_private, null);
            default:
                return new ColorDrawable(Color.TRANSPARENT);
        }

    }

    private String formatHeader(GoogleBookshelf googleBookshelf) {
        return BookUtils.getBookshelfTitle(mContext, googleBookshelf) + " (" + googleBookshelf.getVolumeCount() + ")";
    }

    class BookshelvesViewHolder {
        TextView bookshelfHeader;
        ImageView bookshelfAccess;
        TextView bookshelfDescription;
        LinearLayout llBookshelfDescription;

        BookshelvesViewHolder(View view) {
            bookshelfHeader = view.findViewById(R.id.tvBookshelvesItemHeader);
            bookshelfAccess = view.findViewById(R.id.ivBookshelvesItemAccess);
            bookshelfDescription = view.findViewById(R.id.tvBookshelvesItemDescription);
            llBookshelfDescription = view.findViewById(R.id.llBookshelvesDescription);
        }
    }
}
