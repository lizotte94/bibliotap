package com.bibliotap.entities.google_books;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GoogleBooksVolumeInfo implements Parcelable {

    //region Properties
    @SerializedName("title")
    private String mTitle;

    @SerializedName("subtitle")
    private String mSubTitle;

    @SerializedName("authors")
    private List<String> mAuthors;

    @SerializedName("publisher")
    private String mPublisher;

    @SerializedName("publishedDate")
    private String mPublishedDate;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("industryIdentifiers")
    private List<GoogleBooksIndustryIdentifier> mGoogleBooksIndustryIdentifiers;

    @SerializedName("pageCount")
    private int mPageCount;

    @SerializedName("imageLinks")
    private GoogleBooksImageLinks mImageLinks;

    @SerializedName("language")
    private String mLanguage;

    @SerializedName("previewLink")
    private String mPreviewLink;

    @SerializedName("infoLink")
    private String mInfoLink;
    //endregion

    //region Getters and setters
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getSubTitle() {
        return mSubTitle;
    }

    public void setSubTitle(String subTitle) {
        mSubTitle = subTitle;
    }

    public List<String> getAuthors() {
        return mAuthors;
    }

    public void setAuthors(List<String> authors) {
        mAuthors = authors;
    }

    public String getPublisher() {
        return mPublisher;
    }

    public void setPublisher(String publisher) {
        mPublisher = publisher;
    }

    public String getPublishedDate() {
        return mPublishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        mPublishedDate = publishedDate;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public List<GoogleBooksIndustryIdentifier> getGoogleBooksIndustryIdentifiers() {
        return mGoogleBooksIndustryIdentifiers;
    }

    public void setGoogleBooksIndustryIdentifiers(List<GoogleBooksIndustryIdentifier> googleBooksIndustryIdentifiers) {
        mGoogleBooksIndustryIdentifiers = googleBooksIndustryIdentifiers;
    }

    public int getPageCount() {
        return mPageCount;
    }

    public void setPageCount(int pageCount) {
        mPageCount = pageCount;
    }

    public GoogleBooksImageLinks getImageLinks() {
        return mImageLinks;
    }

    public void setImageLinks(GoogleBooksImageLinks imageLinks) {
        mImageLinks = imageLinks;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

    public String getPreviewLink() {
        return mPreviewLink;
    }

    public void setPreviewLink(String previewLink) {
        mPreviewLink = previewLink;
    }

    public String getInfoLink() {
        return mInfoLink;
    }

    public void setInfoLink(String infoLink) {
        mInfoLink = infoLink;
    }

    //endregion

    //region Parcelable methods
    protected GoogleBooksVolumeInfo(Parcel in) {
        mTitle = in.readString();
        mSubTitle = in.readString();
        if (in.readByte() == 0x01) {
            mAuthors = new ArrayList<String>();
            in.readList(mAuthors, String.class.getClassLoader());
        } else {
            mAuthors = null;
        }
        mPublisher = in.readString();
        mPublishedDate = in.readString();
        mDescription = in.readString();
        if (in.readByte() == 0x01) {
            mGoogleBooksIndustryIdentifiers = new ArrayList<GoogleBooksIndustryIdentifier>();
            in.readList(mGoogleBooksIndustryIdentifiers, GoogleBooksIndustryIdentifier.class.getClassLoader());
        } else {
            mGoogleBooksIndustryIdentifiers = null;
        }
        mPageCount = in.readInt();
        mImageLinks = (GoogleBooksImageLinks) in.readValue(GoogleBooksImageLinks.class.getClassLoader());
        mLanguage = in.readString();
        mPreviewLink = in.readString();
        mInfoLink = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mSubTitle);
        if (mAuthors == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mAuthors);
        }
        dest.writeString(mPublisher);
        dest.writeString(mPublishedDate);
        dest.writeString(mDescription);
        if (mGoogleBooksIndustryIdentifiers == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mGoogleBooksIndustryIdentifiers);
        }
        dest.writeInt(mPageCount);
        dest.writeValue(mImageLinks);
        dest.writeString(mLanguage);
        dest.writeString(mPreviewLink);
        dest.writeString(mInfoLink);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GoogleBooksVolumeInfo> CREATOR = new Parcelable.Creator<GoogleBooksVolumeInfo>() {
        @Override
        public GoogleBooksVolumeInfo createFromParcel(Parcel in) {
            return new GoogleBooksVolumeInfo(in);
        }

        @Override
        public GoogleBooksVolumeInfo[] newArray(int size) {
            return new GoogleBooksVolumeInfo[size];
        }
    };
    //endregion
}
