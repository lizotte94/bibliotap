package com.bibliotap.entities.google_books;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class GoogleBooksImageLinks implements Parcelable {

    @SerializedName("smallThumbnail")
    private String mSmallThumbnail;

    @SerializedName("thumbnail")
    private String mThumbnail;

    //region Getters and setters

    public String getSmallThumbnail() {
        return mSmallThumbnail;
    }

    public void setSmallThumbnail(String smallThumbnail) {
        mSmallThumbnail = smallThumbnail;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String thumbnail) {
        mThumbnail = thumbnail;
    }

    //endregion

    //region Constructors

    public GoogleBooksImageLinks() { }

    public GoogleBooksImageLinks(String thumnbail) {
        this.mThumbnail = thumnbail;
    }

    public GoogleBooksImageLinks(String mSmallThumbnail, String mThumbnail) {
        this.mSmallThumbnail = mSmallThumbnail;
        this.mThumbnail = mThumbnail;
    }
    //endregion

    //region Parcelable methods
    protected GoogleBooksImageLinks(Parcel in) {
        mSmallThumbnail = in.readString();
        mThumbnail = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mSmallThumbnail);
        dest.writeString(mThumbnail);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GoogleBooksImageLinks> CREATOR = new Parcelable.Creator<GoogleBooksImageLinks>() {
        @Override
        public GoogleBooksImageLinks createFromParcel(Parcel in) {
            return new GoogleBooksImageLinks(in);
        }

        @Override
        public GoogleBooksImageLinks[] newArray(int size) {
            return new GoogleBooksImageLinks[size];
        }
    };
    //endregion
}
