package com.bibliotap.entities.google_books;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GoogleBooksRequestResult implements Parcelable {

    @SerializedName("kind")
    private String mKind;

    @SerializedName("totalItems")
    private int mTotalItems;

    @SerializedName("items")
    private List<GoogleBooksVolume> mVolumes;

    //region Getters and setters

    public String getKind() {
        return mKind;
    }

    public void setKind(String kind) {
        mKind = kind;
    }

    public int getTotalItems() {
        return mTotalItems;
    }

    public void setTotalItems(int totalItems) {
        mTotalItems = totalItems;
    }

    public List<GoogleBooksVolume> getVolumes() {
        return mVolumes;
    }

    public void setVolumes(List<GoogleBooksVolume> volumes) {
        mVolumes = volumes;
    }

    //endregion

    protected GoogleBooksRequestResult(Parcel in) {
        mKind = in.readString();
        mTotalItems = in.readInt();
        if (in.readByte() == 0x01) {
            mVolumes = new ArrayList<GoogleBooksVolume>();
            in.readList(mVolumes, GoogleBooksVolume.class.getClassLoader());
        } else {
            mVolumes = null;
        }
    }

    //region Parcelable methods
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mKind);
        dest.writeInt(mTotalItems);
        if (mVolumes == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mVolumes);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GoogleBooksRequestResult> CREATOR = new Parcelable.Creator<GoogleBooksRequestResult>() {
        @Override
        public GoogleBooksRequestResult createFromParcel(Parcel in) {
            return new GoogleBooksRequestResult(in);
        }

        @Override
        public GoogleBooksRequestResult[] newArray(int size) {
            return new GoogleBooksRequestResult[size];
        }
    };
    //endregion
}
