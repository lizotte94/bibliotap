package com.bibliotap.entities.google_books;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class GoogleBooksIndustryIdentifier implements Parcelable {

    @SerializedName("type")
    private String mType;

    @SerializedName("identifier")
    private String mIdentifier;



    //region Getters and setters

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getIdentifier() {
        return mIdentifier;
    }

    public void setIdentifier(String identifier) {
        mIdentifier = identifier;
    }


    //endregion

    //region Constructors

    public GoogleBooksIndustryIdentifier() {}

    public GoogleBooksIndustryIdentifier(String mType, String mIdentifier) {
        this.mType = mType;
        this.mIdentifier = mIdentifier;
    }
    //endregion

    //region Parcelable methods
    protected GoogleBooksIndustryIdentifier(Parcel in) {
        mType = in.readString();
        mIdentifier = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mType);
        dest.writeString(mIdentifier);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GoogleBooksIndustryIdentifier> CREATOR = new Parcelable.Creator<GoogleBooksIndustryIdentifier>() {
        @Override
        public GoogleBooksIndustryIdentifier createFromParcel(Parcel in) {
            return new GoogleBooksIndustryIdentifier(in);
        }

        @Override
        public GoogleBooksIndustryIdentifier[] newArray(int size) {
            return new GoogleBooksIndustryIdentifier[size];
        }
    };
    //endregion

}
