package com.bibliotap.entities.google_books;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class GoogleBooksVolume implements Parcelable {

    //region Properties
    @SerializedName("id")
    private String mId;

    @SerializedName("selfLink")
    private String mSelfLink;

    @SerializedName("volumeInfo")
    private GoogleBooksVolumeInfo mVolumeInfo;
    //endregion

    //region Getters and setters
    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getSelfLink() {
        return mSelfLink;
    }

    public void setSelfLink(String selfLink) {
        mSelfLink = selfLink;
    }

    public GoogleBooksVolumeInfo getVolumeInfo() {
        return mVolumeInfo;
    }

    public void setVolumeInfo(GoogleBooksVolumeInfo volumeInfo) {
        mVolumeInfo = volumeInfo;
    }
    //endregion

    //region Parcelable methods
    protected GoogleBooksVolume(Parcel in) {
        mId = in.readString();
        mSelfLink = in.readString();
        mVolumeInfo = (GoogleBooksVolumeInfo) in.readValue(GoogleBooksVolumeInfo.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mSelfLink);
        dest.writeValue(mVolumeInfo);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GoogleBooksVolume> CREATOR = new Parcelable.Creator<GoogleBooksVolume>() {
        @Override
        public GoogleBooksVolume createFromParcel(Parcel in) {
            return new GoogleBooksVolume(in);
        }

        @Override
        public GoogleBooksVolume[] newArray(int size) {
            return new GoogleBooksVolume[size];
        }
    };
    //endregion
}
