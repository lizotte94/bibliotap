package com.bibliotap.entities.google_books;


import android.content.Context;

import com.bibliotap.main.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GoogleBooksQuery {

    public static final String TITLE = "intitle";
    public static final String AUTHOR = "inauthor";
    public static final String PUBLISHER = "inpublisher";
    public static final String ISBN = "isbn";

    public static final String getFetchedFields() {
        return "kind,totalItems,items(id,selfLink,volumeInfo(title, subtitle, authors,publisher,publishedDate,description,industryIdentifiers/*,pageCount,imageLinks/*,language,previewLink,infoLink))";
    }

    public static String build(String searchBy, String searchValue){

        String queryParameter = "";


        switch(searchBy){
            case GoogleBooksQuery.TITLE:
                queryParameter = searchByTitle(searchValue);
                break;
            case GoogleBooksQuery.AUTHOR:
                queryParameter = searchByAuthor(searchValue);
                break;
            case GoogleBooksQuery.PUBLISHER:
                queryParameter = searchByPublisher(searchValue);
                break;
            case GoogleBooksQuery.ISBN:
                queryParameter = searchByISBN(searchValue);
                break;
        }

        return queryParameter;
    }

    private static String searchByTitle(String title) {
        return GoogleBooksQuery.TITLE + ":" + title;
    }

    private static String searchByAuthor(String author) {
        return GoogleBooksQuery.AUTHOR + ":" + '"' + author + '"';
    }

    private static String searchByPublisher (String publisher) {
        return GoogleBooksQuery.PUBLISHER + ":" + publisher;
    }

    private static String searchByISBN(String isbn) {
        return GoogleBooksQuery.ISBN + ":" + isbn;
    }

    public static List<String> getGoogleSearchFieldsList(Context context) {
        ArrayList<String> items = new ArrayList<>();

        for (Map.Entry<String, String> entry : GoogleBooksQuery.getGoogleSearchFieldsMap(context).entrySet())
            items.add(entry.getKey());

        return items;
    }

    public static LinkedHashMap<String, String> getGoogleSearchFieldsMap(Context context) {
        LinkedHashMap<String, String> searchFields = new LinkedHashMap<>();

        searchFields.put(context.getResources().getString(R.string.search_field_title), GoogleBooksQuery.TITLE);
        searchFields.put(context.getResources().getString(R.string.search_field_author), GoogleBooksQuery.AUTHOR);
        searchFields.put(context.getResources().getString(R.string.search_field_publisher), GoogleBooksQuery.PUBLISHER);
        searchFields.put(context.getResources().getString(R.string.search_field_isbn), GoogleBooksQuery.ISBN);

        return searchFields;
    }
}
