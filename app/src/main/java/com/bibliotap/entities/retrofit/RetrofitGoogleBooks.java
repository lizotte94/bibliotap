package com.bibliotap.entities.retrofit;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
    Le code pour l'Interceptor a été pris sur internet (https://stackoverflow.com/questions/32514410/logging-with-retrofit-2)
     Il permet d'intercepter des requêtes dans le Logcat afin de voir ce qui est envoyé et reçu.
     Je m'en sers pour voir le contenu des requêtes à des fins de débuggage.
 */

public class RetrofitGoogleBooks {

    private static Retrofit mRetrofit;

    private RetrofitGoogleBooks() {}

    public static Retrofit getInstance() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
// add your other interceptors …

// add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!

        if(mRetrofit == null){
            mRetrofit = new Retrofit.Builder()
                    .baseUrl("https://www.googleapis.com/books/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }

        return mRetrofit;
    }
}
