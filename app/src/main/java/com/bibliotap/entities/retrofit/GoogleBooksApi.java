package com.bibliotap.entities.retrofit;

import com.bibliotap.entities.google_books.GoogleBooksRequestResult;
import com.bibliotap.entities.google_bookshelves.GoogleBookshelfRequestResult;
import com.bibliotap.entities.google_bookshelves.GoogleBookshelvesRequestResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface GoogleBooksApi {

    @GET("volumes")
    Call<GoogleBooksRequestResult> getGoogleVolumes(
            @Query("q") String query,
            @Query("fields") String fields,
            @Query("key") String apiKey,
            @Query("maxResults") int maxResults);

    @GET("volumes")
    Call<GoogleBooksRequestResult> getMoreGoogleVolumes(
            @Query("q") String query,
            @Query("fields") String fields,
            @Query("key") String apiKey,
            @Query("maxResults") int maxResults,
            @Query("startIndex") int startIndex);


    @GET("mylibrary/bookshelves")
    Call<GoogleBookshelvesRequestResult> getGooglebookshelves(
            @Header("Authorization") String UserIdToken,
            @Query("key") String apiKey
    );

    @GET("mylibrary/bookshelves/{shelf}/volumes")
    Call<GoogleBookshelfRequestResult> getGooglebookshelfVolumes(
            @Path ("shelf") long bookshelfId,
            @Query("fields") String fields,
            @Header("Authorization") String UserIdToken,
            @Query("key") String apiKey
    );

    @POST("mylibrary/bookshelves/{shelf}/addVolume")
    @Headers("Content-Type: application/json")
    Call<Void> addVolumeToBookshelf(
            @Path ("shelf") long bookshelfId,
            @Query("volumeId") String volumeId,
            @Header("Authorization") String UserIdToken,
            @Query("key") String apiKey
    );

    @POST("mylibrary/bookshelves/{shelf}/removeVolume")
    @Headers("Content-Type: application/json")
    Call<Void> removeVolumeFromBookshelf(
            @Path ("shelf") long bookshelfId,
            @Query("volumeId") String volumeId,
            @Header("Authorization") String UserIdToken,
            @Query("key") String apiKey
    );
}
