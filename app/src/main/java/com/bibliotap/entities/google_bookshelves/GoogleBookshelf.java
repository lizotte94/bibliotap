package com.bibliotap.entities.google_bookshelves;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class GoogleBookshelf implements Parcelable {

    //region Properties
    @SerializedName("kind")
    private String mKind;

    @SerializedName("id")
    private int mId;

    @SerializedName("selfLink")
    private String mSelfLink;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("access")
    private String mAccess;

    @SerializedName("updated")
    private String mUpdated;

    @SerializedName("created")
    private String mCreated;

    @SerializedName("volumeCount")
    private int mVolumeCount;

    @SerializedName("volumesLastUpdated")
    private String mVolumesLastUpdated;
    //endregion

    //region Getters and setters
    public String getKind() {
        return mKind;
    }

    public void setKind(String kind) {
        mKind = kind;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getSelfLink() {
        return mSelfLink;
    }

    public void setSelfLink(String selfLink) {
        mSelfLink = selfLink;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getAccess() {
        return mAccess;
    }

    public void setAccess(String access) {
        mAccess = access;
    }

    public String getUpdated() {
        return mUpdated;
    }

    public void setUpdated(String updated) {
        mUpdated = updated;
    }

    public String getCreated() {
        return mCreated;
    }

    public void setCreated(String created) {
        mCreated = created;
    }

    public int getVolumeCount() {
        return mVolumeCount;
    }

    public void setVolumeCount(int volumeCount) {
        mVolumeCount = volumeCount;
    }

    public String getVolumesLastUpdated() {
        return mVolumesLastUpdated;
    }

    public void setVolumesLastUpdated(String volumesLastUpdated) {
        mVolumesLastUpdated = volumesLastUpdated;
    }
    //endregion

    //region Parcelable methods
    protected GoogleBookshelf(Parcel in) {
        mKind = in.readString();
        mId = in.readInt();
        mSelfLink = in.readString();
        mTitle = in.readString();
        mDescription = in.readString();
        mAccess = in.readString();
        mUpdated = in.readString();
        mCreated = in.readString();
        mVolumeCount = in.readInt();
        mVolumesLastUpdated = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mKind);
        dest.writeInt(mId);
        dest.writeString(mSelfLink);
        dest.writeString(mTitle);
        dest.writeString(mDescription);
        dest.writeString(mAccess);
        dest.writeString(mUpdated);
        dest.writeString(mCreated);
        dest.writeInt(mVolumeCount);
        dest.writeString(mVolumesLastUpdated);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GoogleBookshelf> CREATOR = new Parcelable.Creator<GoogleBookshelf>() {
        @Override
        public GoogleBookshelf createFromParcel(Parcel in) {
            return new GoogleBookshelf(in);
        }

        @Override
        public GoogleBookshelf[] newArray(int size) {
            return new GoogleBookshelf[size];
        }
    };
    //endregion
}