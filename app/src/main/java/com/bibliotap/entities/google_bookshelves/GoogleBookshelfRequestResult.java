package com.bibliotap.entities.google_bookshelves;

import android.os.Parcel;
import android.os.Parcelable;

import com.bibliotap.entities.google_books.GoogleBooksVolume;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GoogleBookshelfRequestResult implements Parcelable {

    //region Properties
    @SerializedName("kind")
    private String mKind;

    @SerializedName("items")
    private List<GoogleBooksVolume> mGoogleBooksVolumes;

    @SerializedName("totalItems")
    private int mTotalItems;
    //endregion

    //region Getters and setters
    public String getKind() {
        return mKind;
    }

    public void setKind(String kind) {
        mKind = kind;
    }

    public List<GoogleBooksVolume> getGoogleBooksVolumes() {
        return mGoogleBooksVolumes;
    }

    public void setGoogleBooksVolumes(List<GoogleBooksVolume> googleBooksVolumes) {
        mGoogleBooksVolumes = googleBooksVolumes;
    }

    public int getTotalItems() {
        return mTotalItems;
    }

    public void setTotalItems(int totalItems) {
        mTotalItems = totalItems;
    }
    //endregion

    //region Parcelable methods
    protected GoogleBookshelfRequestResult(Parcel in) {
        mKind = in.readString();
        if (in.readByte() == 0x01) {
            mGoogleBooksVolumes = new ArrayList<GoogleBooksVolume>();
            in.readList(mGoogleBooksVolumes, GoogleBooksVolume.class.getClassLoader());
        } else {
            mGoogleBooksVolumes = null;
        }
        mTotalItems = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mKind);
        if (mGoogleBooksVolumes == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mGoogleBooksVolumes);
        }
        dest.writeInt(mTotalItems);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GoogleBookshelfRequestResult> CREATOR = new Parcelable.Creator<GoogleBookshelfRequestResult>() {
        @Override
        public GoogleBookshelfRequestResult createFromParcel(Parcel in) {
            return new GoogleBookshelfRequestResult(in);
        }

        @Override
        public GoogleBookshelfRequestResult[] newArray(int size) {
            return new GoogleBookshelfRequestResult[size];
        }
    };
    //endregion
}