package com.bibliotap.entities.google_bookshelves;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GoogleBookshelvesRequestResult implements Parcelable {

    //region Properties
    @SerializedName("kind")
    private String mKind;

    @SerializedName("items")
    private List<GoogleBookshelf> mBookShelves;

    @SerializedName("totalItems")
    private int mTotalItems;
    //endregion

    //region Getters and setters
    public String getKind() {
        return mKind;
    }

    public void setKind(String kind) {
        mKind = kind;
    }

    public List<GoogleBookshelf> getBookShelves() {
        return mBookShelves;
    }

    public void setBookShelves(List<GoogleBookshelf> bookShelves) {
        mBookShelves = bookShelves;
    }

    public int getTotalItems() {
        return mTotalItems;
    }

    public void setTotalItems(int totalItems) {
        mTotalItems = totalItems;
    }
    //endregion

    //region Parcelable methods
    protected GoogleBookshelvesRequestResult(Parcel in) {
        mKind = in.readString();
        if (in.readByte() == 0x01) {
            mBookShelves = new ArrayList<GoogleBookshelf>();
            in.readList(mBookShelves, GoogleBookshelf.class.getClassLoader());
        } else {
            mBookShelves = null;
        }
        mTotalItems = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mKind);
        if (mBookShelves == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mBookShelves);
        }
        dest.writeInt(mTotalItems);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GoogleBookshelvesRequestResult> CREATOR = new Parcelable.Creator<GoogleBookshelvesRequestResult>() {
        @Override
        public GoogleBookshelvesRequestResult createFromParcel(Parcel in) {
            return new GoogleBookshelvesRequestResult(in);
        }

        @Override
        public GoogleBookshelvesRequestResult[] newArray(int size) {
            return new GoogleBookshelvesRequestResult[size];
        }
    };
    //endregion
}
