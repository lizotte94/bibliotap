package com.bibliotap.main;

import android.accounts.AccountManager;
import android.os.Bundle;
import android.view.MenuItem;

import com.bibliotap.bookshelves.BookshelvesAuthenticatedFragment;
import com.bibliotap.bookshelves.BookshelvesUnauthenticatedFragment;
import com.bibliotap.scan.ScanFragment;
import com.bibliotap.search.SearchFragment;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView mBottomNav;
    private int mSelectedFragmentId;
    public Fragment mSelectedFragment;

    /*
    On regarde tout d'abord à la création si l'utilisateur a été redirigé (à partir d'un autre activité) afin de se connecter.
    Si oui, on  ouvre le fragment Bookshelves pour l'inviter à se connecter.
    Si non, on vérifie s'il y a un fragment sauvegardé dans le backstack.
    S'il n'y en a pas, on ouvre le scanner. S'il y en a un, on le récupère et l'affiche.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBottomNav = findViewById(R.id.nav_bottom);
        mBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                displayView(menuItem.getItemId());
                mSelectedFragmentId = menuItem.getItemId();
                return true;
            }
        });

        if(getIntent().getBooleanExtra("redirectedToConnect", false)){
            mSelectedFragmentId = R.id.nav_bottom_bookshelves_item;
            mBottomNav.setSelectedItemId(mSelectedFragmentId);
            displayView(mSelectedFragmentId);
        }
        else{
            if(savedInstanceState == null){
                mSelectedFragmentId = R.id.nav_bottom_scan_item;
                mBottomNav.setSelectedItemId(mSelectedFragmentId);
                displayView(mSelectedFragmentId);
            }
            else {
                mSelectedFragmentId = savedInstanceState.getInt("mSelectedFragmentId");
                mSelectedFragment = getSupportFragmentManager().getFragment(savedInstanceState, "selectedFragment");
                displayView(mSelectedFragmentId);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("mSelectedFragmentId", mSelectedFragmentId);
        getSupportFragmentManager().putFragment(outState, "selectedFragment", mSelectedFragment);
    }

    private void displayView(int viewId){
        if(mSelectedFragment != null && mSelectedFragmentId == viewId){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame_main_activity, mSelectedFragment);
            transaction.commit();
            return;
        }

        AccountManager a = AccountManager.get(this);


        Fragment fragment = null;

        switch(viewId){
            case R.id.nav_bottom_bookshelves_item:
                if(GoogleSignIn.getLastSignedInAccount(this) == null)
                    fragment = new BookshelvesUnauthenticatedFragment();
                else
                    fragment = BookshelvesAuthenticatedFragment.newInstance(GoogleSignIn.getLastSignedInAccount(this));
                break;

            case R.id.nav_bottom_scan_item:
                fragment = new ScanFragment();
                break;

            case R.id.nav_bottom_search_item:
                fragment = new SearchFragment();
                break;
        }

        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            mSelectedFragment = fragment;
            transaction.replace(R.id.content_frame_main_activity, mSelectedFragment);
            transaction.commit();
        }
    }
}