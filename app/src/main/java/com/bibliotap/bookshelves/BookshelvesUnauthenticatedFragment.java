package com.bibliotap.bookshelves;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bibliotap.assets.Keys;
import com.bibliotap.main.MainActivity;
import com.bibliotap.main.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.Task;

/*
    Ce fragment permet de s'authentifier par l'API de Google SignIn.
    Le fonctionnement est assez simple :

        1 - On spéficie, avec GoogleSignInOptions, ce qui est demandé lors de la connexion (Email, Id, gérer les livres, etc).
        2 - On récupère, avec GoogleSignInClient, le client, fournit par l'API de Google Sign In, qui s'occupera
            d'afficher la demande de connexion et permettra de sélectionner un compte.
        3 - onActivityResult, on vérifie si le compte est connecté.
        4 - Si oui, handleSignInResult permet de gérer ce qu'il faut faire, dans ce cas-ci,
            on appelle displayBookshelves.

    Dans displayBookshelves, on vérifie que le compte est bien existant et on créer le
    fragment BookshelvesAuthenticatedFragment en informant l'activité que c'est maintenant
    le fragment à afficher (Car sinon, c'est ce fragment-ci qui reste dans le backstack et il y aura
    erreur, lorsque l'utilisateur reviendra dans l'onglet Bookshelves, car MainActivity tentera
    de récupérer un fragment qui ne sera plus dans le FragmentManager).

 */

public class BookshelvesUnauthenticatedFragment extends Fragment {

    private final static int RC_SIGN_IN = 777;

    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInAccount mGoogleSignInAccount;

    public BookshelvesUnauthenticatedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestId()
                .requestEmail()
                .requestScopes(new Scope("https://www.googleapis.com/auth/books"))
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);

        mGoogleSignInAccount = GoogleSignIn.getLastSignedInAccount(getContext());
    }

    @Override
    public void onStart() {
        super.onStart();

        if(mGoogleSignInAccount != null){
            displayBookshelves(mGoogleSignInAccount);
        }
        else {
            TextView googleConnectText = getActivity().findViewById(R.id.tvGoogleConnectText);
            String text = getText(R.string.bookshelves_google_connect).toString();
            Spanned styledText = Html.fromHtml(text);
            googleConnectText.setText(styledText);

            SignInButton signInButton = getActivity().findViewById(R.id.sign_in_button);
            signInButton.setSize(SignInButton.SIZE_WIDE);
            signInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signIn();
                }
            });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bookshelves_unauthenticated, container, false);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            displayBookshelves(account);
        } catch (ApiException e) {
            Log.w("GoogleSignIn", "signInResult:failed code=" + e.getStatusCode());
            displayBookshelves(null);
        }
    }

    private void displayBookshelves(GoogleSignInAccount googleSignInAccount){
        if(googleSignInAccount != null){
            Fragment fragment = BookshelvesAuthenticatedFragment.newInstance(googleSignInAccount);
            ((MainActivity) getActivity()).mSelectedFragment = fragment;
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame_main_activity, fragment);
            transaction.commit();
        }
        else{
            Toast.makeText(getContext(), R.string.bookshelves_google_connect_failed, Toast.LENGTH_LONG).show();
        }
    }
}
