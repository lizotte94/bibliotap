package com.bibliotap.bookshelves;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.bibliotap.adapters.BookshelvesAdapter;
import com.bibliotap.assets.Keys;
import com.bibliotap.entities.google_bookshelves.GoogleBookshelvesRequestResult;
import com.bibliotap.entities.retrofit.GoogleBooksApi;
import com.bibliotap.entities.retrofit.RetrofitGoogleBooks;
import com.bibliotap.main.R;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookshelvesAuthenticatedFragment extends Fragment implements GoogleBooksToken.AsyncResponse {

    private static final String GOOGLE_SIGN_IN_ACCOUNT = "googleSignInAccount";

    private GoogleSignInAccount mGoogleSignInAccount;
    private GoogleBookshelvesRequestResult mGoogleBookshelvesRequestResult;

    public BookshelvesAuthenticatedFragment() {
        // Required empty public constructor
    }

    public static BookshelvesAuthenticatedFragment newInstance(GoogleSignInAccount googleSignInAccount) {
        BookshelvesAuthenticatedFragment fragment = new BookshelvesAuthenticatedFragment();
        Bundle args = new Bundle();
        args.putParcelable(GOOGLE_SIGN_IN_ACCOUNT, googleSignInAccount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mGoogleSignInAccount = getArguments().getParcelable(GOOGLE_SIGN_IN_ACCOUNT);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        getToken();
    }

    private void getToken(){
        new GoogleBooksToken(this, getActivity(), mGoogleSignInAccount.getAccount()).execute();
    }

    @Override
    public void getTokenCallback(String token) {
        fetchBookshelves(token);
    }

    private void fetchBookshelves(String token) {
        GoogleBooksApi googleBooksApi = RetrofitGoogleBooks.getInstance().create(GoogleBooksApi.class);

        Call<GoogleBookshelvesRequestResult> getGooglebookshelves =
                googleBooksApi.getGooglebookshelves("Bearer " + token, Keys.GOOGLE_BOOKS_API);

        getGooglebookshelves.enqueue(new Callback<GoogleBookshelvesRequestResult>() {
            @Override
            public void onResponse(Call<GoogleBookshelvesRequestResult> call, Response<GoogleBookshelvesRequestResult> response) {
                mGoogleBookshelvesRequestResult = response.body();
                showBookshelves();
            }

            @Override
            public void onFailure(Call<GoogleBookshelvesRequestResult> call, Throwable t) {
                Toast.makeText(getContext(), R.string.bookshelves_error_fetch, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showBookshelves() {
        if(getActivity() != null && mGoogleBookshelvesRequestResult != null){
            ListView lvBookshelves = getActivity().findViewById(R.id.lvBookshelves);
            BookshelvesAdapter adapter = new BookshelvesAdapter(getActivity(), R.layout.bookshelves_item, mGoogleBookshelvesRequestResult.getBookShelves());
            lvBookshelves.setAdapter(adapter);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bookshelves_authenticated, container, false);
    }
}