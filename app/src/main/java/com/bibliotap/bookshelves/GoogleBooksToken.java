package com.bibliotap.bookshelves;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import java.io.IOException;
import java.lang.ref.WeakReference;

/*
----------
    La fonctionnalité décrite ci-dessous a temporairement été commentée, car elle semble causer problème.
    En effet, le token n'est parfois pas bien rafraichit lorsque l'application est inutilisée pendant quelques temps.
    C'est probablement soit un problème avec l'authentification au compte Google (getLastsignedAccount) ou encore
    un problème relié au SharePreferences que j'utilise peut-être mal. À retravailler et tester.
----------

   Cette classe a été créée afin d'éviter de spammer l'API de Auth 2.0.
   Puisqu'il est nécéssaire d'envoyer un token lors de plusieurs requêtes, on stock
   le token afin de le réutiliser.

   Il expire normalement après 60 minutes, mais on le rafraichit au 30 minutes.

   Le SharePreferences est utilisé car il est persistent, même lorsque l'application
   est fermée et réouverte. On stock le token avec un temps d'expiration, lors d'une
   requête on vérifie si le token est expiré.
   Si non, on utilise le restoredToken.
   Si oui, on en demande un nouveau et on le stock dans les SharedPreferences.
 */

public class GoogleBooksToken extends AsyncTask<Void, Void, String> {

    public interface AsyncResponse {
        void getTokenCallback(String output);
    }

    private static final String SP_TOKEN = "GoogleBooksAuthToken";
    private static final String SP_TOKEN_EXPIRATION = "GoogleBooksAuthTokenExpiration";

    private AsyncResponse mDelegate;
    private WeakReference<Activity> mWeakReferenceActivity;
    private Account mGoogleAccount;


    public GoogleBooksToken(AsyncResponse delegate, Activity activity, Account googleAccount) {
        mWeakReferenceActivity = new WeakReference<>(activity);
        mGoogleAccount = googleAccount;
        mDelegate = delegate;
    }

    @Override
    protected String doInBackground(Void... params) {

        String token = "";

        if(mWeakReferenceActivity.get() != null){

//            SharedPreferences prefs =
//                    mWeakReferenceActivity.get().getSharedPreferences(SP_TOKEN, Context.MODE_PRIVATE);
//
//            String restoredToken = prefs.getString("token", null);
//            long restoredTokenExpiration = prefs.getLong(SP_TOKEN_EXPIRATION, 0);
//
//            if (restoredToken != null && System.currentTimeMillis() < restoredTokenExpiration) {
//                return restoredToken;
//            }
//            else {
                try {
                    token = GoogleAuthUtil.getToken(mWeakReferenceActivity.get(), mGoogleAccount, "oauth2:https://www.googleapis.com/auth/books");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (GoogleAuthException e) {
                    e.printStackTrace();
                }
            }

            //On ne veut pas stocker un Token vide.
//            if(!token.isEmpty()){
//                SharedPreferences.Editor editor = mWeakReferenceActivity.get()
//                        .getSharedPreferences(SP_TOKEN, Context.MODE_PRIVATE).edit();
//                editor.putString("token", token);
//                editor.putLong(SP_TOKEN_EXPIRATION, System.currentTimeMillis() + 1800000);
//                editor.apply();
//            }
//        }

        return token;
    }

    @Override
    protected void onPostExecute(String token) {
        mDelegate.getTokenCallback(token);
    }

    public static void resetToken(Activity activity){
        SharedPreferences.Editor editor = activity
                .getSharedPreferences(SP_TOKEN, Context.MODE_PRIVATE).edit();
        editor.putString("token", null);
        editor.apply();
    }
}

