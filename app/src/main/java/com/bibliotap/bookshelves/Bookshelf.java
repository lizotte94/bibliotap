package com.bibliotap.bookshelves;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bibliotap.adapters.BookshelfAdapter;
import com.bibliotap.assets.Keys;
import com.bibliotap.entities.google_books.GoogleBooksQuery;
import com.bibliotap.entities.google_books.GoogleBooksVolume;
import com.bibliotap.entities.google_bookshelves.GoogleBookshelf;
import com.bibliotap.entities.google_bookshelves.GoogleBookshelfRequestResult;
import com.bibliotap.entities.retrofit.GoogleBooksApi;
import com.bibliotap.entities.retrofit.RetrofitGoogleBooks;
import com.bibliotap.main.R;
import com.bibliotap.utils.BookUtils;
import com.google.android.gms.auth.api.signin.GoogleSignIn;

import java.util.List;

public class Bookshelf extends AppCompatActivity implements GoogleBooksToken.AsyncResponse {

    private RecyclerView rvBookshelf;
    private ImageButton mBackButton;
    private TextView mNavTopTitle;

    private GoogleBookshelf mGoogleBookshelf;
    private GoogleBookshelfRequestResult mGoogleBookshelfRequestResult;
    private List<GoogleBooksVolume> mGoogleBooksVolumes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookshelf);

        if(getIntent().getParcelableExtra("googleBookshelf") != null){
            mGoogleBookshelf = getIntent().getParcelableExtra("googleBookshelf");
        }

        mBackButton = findViewById(R.id.btnBookshelfBack);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mNavTopTitle = findViewById(R.id.tvBookshelfNavTopTitle);


        rvBookshelf = findViewById(R.id.rvBookshelf);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getToken();
    }

    private void getToken(){
        new GoogleBooksToken(this, this, GoogleSignIn.getLastSignedInAccount(this).getAccount()).execute();
    }

    @Override
    public void getTokenCallback(String token) {
        fetchBookshelf(token);
    }

    private void fetchBookshelf(String token) {
        GoogleBooksApi googleBooksApi = RetrofitGoogleBooks.getInstance().create(GoogleBooksApi.class);

        Call<GoogleBookshelfRequestResult> getGooglebookshelfVolumes =
                googleBooksApi.getGooglebookshelfVolumes(mGoogleBookshelf.getId(), GoogleBooksQuery.getFetchedFields(),"Bearer " + token, Keys.GOOGLE_BOOKS_API);

        getGooglebookshelfVolumes.enqueue(new Callback<GoogleBookshelfRequestResult>() {
            @Override
            public void onResponse(Call<GoogleBookshelfRequestResult> call, Response<GoogleBookshelfRequestResult> response) {
                mGoogleBookshelfRequestResult = response.body();

                if(mGoogleBookshelfRequestResult != null && mGoogleBookshelfRequestResult.getTotalItems() > 0){
                    mGoogleBooksVolumes = mGoogleBookshelfRequestResult.getGoogleBooksVolumes();
                    showBookshelf();
                }
                    setNavTopTitle();
            }

            @Override
            public void onFailure(Call<GoogleBookshelfRequestResult> call, Throwable t) {
                Toast.makeText(getApplicationContext(), R.string.bookshelf_error_fetch, Toast.LENGTH_LONG).show();
            }
        });
    }

     public void setNavTopTitle() {
        int numberOfVolumes = 0;
        if(mGoogleBooksVolumes != null)
            numberOfVolumes = mGoogleBooksVolumes.size();

        mNavTopTitle.setText(getString(R.string.nav_top_bookshelf, BookUtils.getBookshelfTitle(getApplicationContext(), mGoogleBookshelf), numberOfVolumes));
    }

    private void showBookshelf() {
        rvBookshelf.setLayoutManager(new LinearLayoutManager(this));
        BookshelfAdapter adapter = new BookshelfAdapter(this, mGoogleBookshelf, mGoogleBooksVolumes);
        rvBookshelf.setAdapter(adapter);
        registerForContextMenu(rvBookshelf);
    }
}
